# What this is about
Exercise assignment for the course of Distributed and Concurrent Programming (Programmazione Concorrente e Distribuita) AA. 2017/2018 - Exam session

```Due delivery: February 8, 2018. h.23:59```

#Participants
This exercise concerns ONLY those students which have not delivered either the first or the second set of exercises.
In addition to THIS exercise, the student should ALSO deliver the other missing exercise(s).

Those students which have not delivered the first set of exercises, considering that a solution has already been published, should instead deliver a short report.txt highlighting the main points of this exercise.

#Getting Started

##Environment

The student might import the project template in their preferred IDE. No build system config. files have been provided, the student might employ any such system depending on his/her preferences.

The code is commented and you should fill in the missing implementation. No test cases are provided for this delivery. You can rely, introduce auxiliary methods in addition to those already provided.

##The Exercise

Consists of a single-node MapReduce-like implementation for the word counting 'problem'. The library provides different input modalities (file and network) employed to retrieve a dataset consisting of a bag of words. Once the input has been formatted it is then fed to the MapReduce
job which performs the necessary computation, producing in output a file containing the words along with their frequency. The produced output is SORTED by key (=word). Consider the running example: dataset = B,B,A,A,A,C,C, the produced output is: 
```
A: 3
B: 2
C: 2
```

#Project Delivery

The delivery will happen via email, sent to me following these criteria: (1) The email subject adheres to the following format: [pcd] Name Surname Student_Identifier, the textual contents of the email are not important but you can repeat the subject (e.g., [pcd] pinco palino 123432). (2) The source files comprising the 'src' directory should be wrapped inside a root directory named according to the format name-surname-studentIdentifier (e.g., pinco-pallino-123432) and compressed. Only the compressed file should be sent through email.

Directory structure example:

```
pinco-pallino-123432 
│
|───exerPCDapp1/src
|
└───exerPCDmod(1/2)/src
```

Note that no external archives should be referenced in you project. Failure to meet these criteria can result in you project not being evaluated.

